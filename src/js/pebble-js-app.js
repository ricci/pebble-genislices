var urls = {
    "sc1" : "http://boss.utahddc.geniracks.net/active_experiments.php3",
    "sc2" : "http://boss.emulab.net/active_experiments.php3",
    "sc3" : "http://boss.utah.geniracks.net/active_experiments.php3",
    "sc4" : "http://boss.instageni.gpolab.bbn.com/active_experiments.php3",
    "sc5" : "http://boss.instageni.northwestern.edu/active_experiments.php3",
};

var nextresponse = null;

function sendBatch() {
    console.log("Sending batched response");
    Pebble.sendAppMessage(nextresponse);
    nextresponse = null;
};

function fetchCount(which) {
  if (!urls[which]) {
      return;
  }
  var response;
  var req = new XMLHttpRequest();
  // build the GET request
  var url = urls[which];
  req.open('GET', url, true);
  console.log("Fetching " + which + ": " + url);
  req.onload = function(e) {
     console.log("Response for " + which + ": " + req.readyState.toString());
    if (req.readyState == 4) {
        console.log("Sending response for " + which + ": " + req.responseText);
        if (nextresponse == null) {
            nextresponse = {};
            setTimeout(sendBatch,200);
        }
        nextresponse[which] = req.responseText;
      } else {
        console.log("Request returned error code " + req.status.toString());
      }
  }
  req.send(null);
}

var doit = function(e) {
                      console.log("message");
                      if (e.payload.fetch) {
                        fetchCount("sc1");
                        fetchCount("sc2");
                        fetchCount("sc3");
                        fetchCount("sc4");
                        fetchCount("sc5");
                      }
                    };

var repeatMe = function (e) {
    console.log("RepeatMe called");
    var e = {};
    e["payload"] = {};
    e["payload"]["fetch"] = 1;
    doit(e);
    // Get some, go again
    console.log("RepeatMe scheduling itself in 30 seconds");
    setTimeout(repeatMe,30000);
};

// Set callback for the app ready event
Pebble.addEventListener("ready",
                        function(e) {
                          console.log("connect!" + e.ready);
                          console.log(e.type);
                          setTimeout(repeatMe,30000);
                        });

// Set callback for appmessage events
Pebble.addEventListener("appmessage", doit);

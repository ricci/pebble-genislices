#include <pebble.h>

static Window *window;
static TextLayer *title_layer;
static TextLayer *l1_layer;
static TextLayer *c1_layer;
static TextLayer *d1_layer;
static TextLayer *l2_layer;
static TextLayer *c2_layer;
static TextLayer *d2_layer;
static TextLayer *l3_layer;
static TextLayer *c3_layer;
static TextLayer *d3_layer;
static TextLayer *l4_layer;
static TextLayer *c4_layer;
static TextLayer *d4_layer;
static TextLayer *l5_layer;
static TextLayer *c5_layer;
static TextLayer *d5_layer;
static char sc1[10];
static char sc2[10];
static char sc3[10];
static char sc4[10];
static char sc5[10];

static int firsttime1 = 1;
static int firsttime2 = 1;
static int firsttime3 = 1;
static int firsttime4 = 1;
static int firsttime5 = 1;

#define HEADER_HEIGHT 34
#define MARGIN 3
#define LINE_HEIGHT 23
#define COUNT_WIDTH 25
#define DIFF_WIDTH 10
#define LINE_START(x) (HEADER_HEIGHT + (x-1) * LINE_HEIGHT)
#define LABEL_W (bounds.size.w - COUNT_WIDTH - MARGIN - DIFF_WIDTH)
#define COUNT_W (COUNT_WIDTH - MARGIN)
#define COUNT_START (bounds.size.w - COUNT_WIDTH)
#define DIFF_START (bounds.size.w - COUNT_WIDTH - DIFF_WIDTH)

enum {
  QUOTE_KEY_FETCH = 0x0,
  QUOTE_KEY_SC1   = 0x1,
  QUOTE_KEY_SC2   = 0x2,
  QUOTE_KEY_SC3   = 0x3,
  QUOTE_KEY_SC4   = 0x4,
  QUOTE_KEY_SC5   = 0x5,
};

static void fetch_msg(void) {
  Tuplet fetch_tuple = TupletInteger(QUOTE_KEY_FETCH, 1);
  DictionaryIterator *iter;
  app_message_outbox_begin(&iter);

  if (iter == NULL) {
    return;
  }

  dict_write_tuplet(iter, &fetch_tuple);
  dict_write_end(iter);

  app_message_outbox_send();
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  // refresh
  fetch_msg();
}

void select_long_click_handler(ClickRecognizerRef recognizer, void *context) {
  text_layer_set_text(c1_layer, "...");
  text_layer_set_text(c2_layer, "...");
  text_layer_set_text(c3_layer, "...");
  text_layer_set_text(c4_layer, "...");
  text_layer_set_text(c5_layer, "...");
  text_layer_set_text(d1_layer, "");
  text_layer_set_text(d2_layer, "");
  text_layer_set_text(d3_layer, "");
  text_layer_set_text(d4_layer, "");
  text_layer_set_text(d5_layer, "");
  fetch_msg();
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_long_click_subscribe(BUTTON_ID_SELECT, 700, select_long_click_handler, NULL);
}

static void in_received_handler(DictionaryIterator *iter, void *context) {
  Tuple *sc1_tuple = dict_find(iter, QUOTE_KEY_SC1);
  Tuple *sc2_tuple = dict_find(iter, QUOTE_KEY_SC2);
  Tuple *sc3_tuple = dict_find(iter, QUOTE_KEY_SC3);
  Tuple *sc4_tuple = dict_find(iter, QUOTE_KEY_SC4);
  Tuple *sc5_tuple = dict_find(iter, QUOTE_KEY_SC5);

  APP_LOG(APP_LOG_LEVEL_DEBUG, "In handler");

  if (sc1_tuple) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Response for SC1");
    if (!firsttime1 && strncmp(sc1,sc1_tuple->value->cstring ,10)) {
        vibes_short_pulse();
        text_layer_set_font(c1_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
        if (atoi(sc1_tuple->value->cstring) < atoi(sc1)) {
            text_layer_set_text(d1_layer,"-");
        } else {
            text_layer_set_text(d1_layer,"+");
        }
    } else {
        text_layer_set_font(c1_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
        text_layer_set_text(d1_layer,"");
    }
    firsttime1 = 0;
    strncpy(sc1, sc1_tuple->value->cstring, 10);
    text_layer_set_text(c1_layer, sc1);
  }

  if (sc2_tuple) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Response for SC2");
    if (!firsttime2 && strncmp(sc2,sc2_tuple->value->cstring ,10)) {
        vibes_short_pulse();
        text_layer_set_font(c2_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
        if (atoi(sc2_tuple->value->cstring) < atoi(sc2)) {
            text_layer_set_text(d2_layer,"-");
        } else {
            text_layer_set_text(d2_layer,"+");
        }
    } else {
        text_layer_set_font(c2_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
        text_layer_set_text(d2_layer,"");
    }
    firsttime2 = 0;
    strncpy(sc2, sc2_tuple->value->cstring, 10);
    text_layer_set_text(c2_layer, sc2);
  }

  if (sc3_tuple) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Response for SC3");
    if (!firsttime3 && strncmp(sc3,sc3_tuple->value->cstring ,10)) {
        vibes_short_pulse();
        text_layer_set_font(c3_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
        if (atoi(sc3_tuple->value->cstring) < atoi(sc3)) {
            text_layer_set_text(d3_layer,"-");
        } else {
            text_layer_set_text(d3_layer,"+");
        }
    } else {
        text_layer_set_font(c3_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
        text_layer_set_text(d3_layer,"");
    }
    firsttime3 = 0;
    strncpy(sc3, sc3_tuple->value->cstring, 10);
    text_layer_set_text(c3_layer, sc3);
  }

  if (sc4_tuple) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Response for SC4");
    if (!firsttime4 && strncmp(sc4,sc4_tuple->value->cstring ,10)) {
        vibes_short_pulse();
        text_layer_set_font(c4_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
        if (atoi(sc4_tuple->value->cstring) < atoi(sc4)) {
            text_layer_set_text(d4_layer,"-");
        } else {
            text_layer_set_text(d4_layer,"+");
        }
    } else {
        text_layer_set_font(c4_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
        text_layer_set_text(d4_layer,"");
    }
    firsttime4 = 0;
    strncpy(sc4, sc4_tuple->value->cstring, 10);
    text_layer_set_text(c4_layer, sc4);
  }
  
  if (sc5_tuple) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Response for SC5");
    if (!firsttime5 && strncmp(sc5,sc5_tuple->value->cstring ,10)) {
        vibes_short_pulse();
        text_layer_set_font(c5_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
        if (atoi(sc5_tuple->value->cstring) < atoi(sc5)) {
            text_layer_set_text(d5_layer,"-");
        } else {
            text_layer_set_text(d5_layer,"+");
        }
    } else {
        text_layer_set_font(c5_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
        text_layer_set_text(d5_layer,"");
    }
    firsttime5 = 0;
    strncpy(sc5, sc5_tuple->value->cstring, 10);
    text_layer_set_text(c5_layer, sc5);
  }

}

static void in_dropped_handler(AppMessageResult reason, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Dropped! (%d)",reason);
}

static void out_failed_handler(DictionaryIterator *failed, AppMessageResult reason, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Failed to Send! (%d)",reason);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Trying Again");
  fetch_msg();
}

static void app_message_init(void) {
  // Register message handlers
  app_message_register_inbox_received(in_received_handler);
  app_message_register_inbox_dropped(in_dropped_handler);
  app_message_register_outbox_failed(out_failed_handler);
  // Init buffers
  app_message_open(256, 256);
  //fetch_msg();
}

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  title_layer = text_layer_create(
      (GRect) { .origin = { 0, 0 }, .size = { bounds.size.w, HEADER_HEIGHT } });
  text_layer_set_text(title_layer, "Slice Counts");
  text_layer_set_text_alignment(title_layer, GTextAlignmentCenter);
  text_layer_set_font(title_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
  layer_add_child(window_layer, text_layer_get_layer(title_layer));

  l1_layer = text_layer_create(
      (GRect) { .origin = { MARGIN, LINE_START(1) }, .size = { LABEL_W, LINE_HEIGHT } });
  text_layer_set_text(l1_layer, "Utah DDC"); text_layer_set_text_alignment(l1_layer, GTextAlignmentLeft); text_layer_set_font(l1_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
  layer_add_child(window_layer, text_layer_get_layer(l1_layer));

  c1_layer = text_layer_create(
      (GRect) { .origin = { COUNT_START, LINE_START(1) }, .size = { COUNT_W, LINE_HEIGHT } });
  text_layer_set_text(c1_layer, "...");
  text_layer_set_text_alignment(c1_layer, GTextAlignmentRight);
  text_layer_set_font(c1_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(c1_layer));

  d1_layer = text_layer_create(
      (GRect) { .origin = { DIFF_START, LINE_START(1) }, .size = { DIFF_WIDTH, LINE_HEIGHT } });
  text_layer_set_text(d1_layer, "");
  text_layer_set_text_alignment(d1_layer, GTextAlignmentRight);
  text_layer_set_font(d1_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(d1_layer));

  // Line 2
  l2_layer = text_layer_create(
      (GRect) { .origin = { MARGIN, LINE_START(2) }, .size = { LABEL_W, LINE_HEIGHT } });
  text_layer_set_text(l2_layer, "Emulab"); text_layer_set_text_alignment(l2_layer, GTextAlignmentLeft); text_layer_set_font(l2_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
  layer_add_child(window_layer, text_layer_get_layer(l2_layer));

  c2_layer = text_layer_create(
      (GRect) { .origin = { COUNT_START, LINE_START(2) }, .size = { COUNT_W, LINE_HEIGHT } });
  text_layer_set_text(c2_layer, "...");
  text_layer_set_text_alignment(c2_layer, GTextAlignmentRight);
  text_layer_set_font(c2_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(c2_layer));

  d2_layer = text_layer_create(
      (GRect) { .origin = { DIFF_START, LINE_START(2) }, .size = { DIFF_WIDTH, LINE_HEIGHT } });
  text_layer_set_text(d2_layer, "");
  text_layer_set_text_alignment(d2_layer, GTextAlignmentRight);
  text_layer_set_font(d2_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(d2_layer));

  // Line 3
  l3_layer = text_layer_create(
      (GRect) { .origin = { MARGIN, LINE_START(3) }, .size = { LABEL_W, LINE_HEIGHT } });
  text_layer_set_text(l3_layer, "Utah"); text_layer_set_text_alignment(l3_layer, GTextAlignmentLeft); text_layer_set_font(l3_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
  layer_add_child(window_layer, text_layer_get_layer(l3_layer));

  c3_layer = text_layer_create(
      (GRect) { .origin = { COUNT_START, LINE_START(3) }, .size = { COUNT_W, LINE_HEIGHT } });
  text_layer_set_text(c3_layer, "...");
  text_layer_set_text_alignment(c3_layer, GTextAlignmentRight);
  text_layer_set_font(c3_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(c3_layer));

  d3_layer = text_layer_create(
      (GRect) { .origin = { DIFF_START, LINE_START(3) }, .size = { DIFF_WIDTH, LINE_HEIGHT } });
  text_layer_set_text(d3_layer, "");
  text_layer_set_text_alignment(d3_layer, GTextAlignmentRight);
  text_layer_set_font(d3_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(d3_layer));
  
  // Line 4
  l4_layer = text_layer_create(
      (GRect) { .origin = { MARGIN, LINE_START(4) }, .size = { LABEL_W, LINE_HEIGHT } });
  text_layer_set_text(l4_layer, "BBN"); text_layer_set_text_alignment(l4_layer, GTextAlignmentLeft); text_layer_set_font(l4_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
  layer_add_child(window_layer, text_layer_get_layer(l4_layer));

  c4_layer = text_layer_create(
      (GRect) { .origin = { COUNT_START, LINE_START(4) }, .size = { COUNT_W, LINE_HEIGHT } });
  text_layer_set_text(c4_layer, "...");
  text_layer_set_text_alignment(c4_layer, GTextAlignmentRight);
  text_layer_set_font(c4_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(c4_layer));

  d4_layer = text_layer_create(
      (GRect) { .origin = { DIFF_START, LINE_START(4) }, .size = { DIFF_WIDTH, LINE_HEIGHT } });
  text_layer_set_text(d4_layer, "");
  text_layer_set_text_alignment(d4_layer, GTextAlignmentRight);
  text_layer_set_font(d4_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(d4_layer));
  
  // Line 5
  l5_layer = text_layer_create(
      (GRect) { .origin = { MARGIN, LINE_START(5) }, .size = { LABEL_W, LINE_HEIGHT } });
  text_layer_set_text(l5_layer, "Northwestern"); text_layer_set_text_alignment(l5_layer, GTextAlignmentLeft); text_layer_set_font(l5_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
  layer_add_child(window_layer, text_layer_get_layer(l5_layer));

  c5_layer = text_layer_create(
      (GRect) { .origin = { COUNT_START, LINE_START(5) }, .size = { COUNT_W, LINE_HEIGHT } });
  text_layer_set_text(c5_layer, "...");
  text_layer_set_text_alignment(c5_layer, GTextAlignmentRight);
  text_layer_set_font(c5_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(c5_layer));

  d5_layer = text_layer_create(
      (GRect) { .origin = { DIFF_START, LINE_START(5) }, .size = { DIFF_WIDTH, LINE_HEIGHT } });
  text_layer_set_text(d5_layer, "");
  text_layer_set_text_alignment(d5_layer, GTextAlignmentRight);
  text_layer_set_font(d5_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  layer_add_child(window_layer, text_layer_get_layer(d5_layer));

  fetch_msg();
}

static void window_unload(Window *window) {
  text_layer_destroy(title_layer);
  text_layer_destroy(l1_layer);
  text_layer_destroy(c1_layer);
  text_layer_destroy(d1_layer);
  text_layer_destroy(l2_layer);
  text_layer_destroy(c2_layer);
  text_layer_destroy(d2_layer);
  text_layer_destroy(l3_layer);
  text_layer_destroy(c3_layer);
  text_layer_destroy(d3_layer);
  text_layer_destroy(l4_layer);
  text_layer_destroy(c4_layer);
  text_layer_destroy(d4_layer);
  text_layer_destroy(l5_layer);
  text_layer_destroy(c5_layer);
  text_layer_destroy(d5_layer);
}

static void init(void) {
  window = window_create();
  app_message_init();
  window_set_click_config_provider(window, click_config_provider);
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });
  const bool animated = true;
  window_stack_push(window, animated);
}

static void deinit(void) {
  window_destroy(window);
}

int main(void) {
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);

  app_event_loop();
  deinit();
}
